#!/bin/sh
if "true" : '''\'
then
CLASSPATH=.
exec "./jython2.7-b4/bin/jython" "$0" "$@"
exit 127
fi
'''
import Foo
import Bar

foos = [
    Foo('one'),
    Foo('two'),
    Foo('three')
]

b = Bar()
b.blabla(foos)
