#!/bin/sh
if "true" : '''\'
then
export PYTHONPATH=./rpyc
exec /usr/bin/env python "$0" "$@"
exit 127
fi
'''

import rpyc
jvm = rpyc.classic.connect('localhost')

j_Foo   = jvm.modules.Foo
j_Bar   = jvm.modules.Bar
j_array = jvm.modules.jarray.array

foos = [
    j_Foo('one'),
    j_Foo('two'),
    j_Foo('three')
]

b = j_Bar()
b.blabla(j_array(foos, j_Foo))
